/******************************************************************************
        (c) COPYRIGHT 2017-  by D.C.P  Co.,Ltd
                          All rights reserved.
@file: exceptpub.h
@brief:对于异常模块的结构体系的定义 接口宏的定义 公共函数的定义
@author   D.C.P 			                                                 
@version  0.0.0.1                                                               
@date     2017/03/02
-------------------------------------------------------------------------------
  Change History :                                                              
                 | <Date>      | <Version> | <Author> | <Description>                     
-------------------------------------------------------------------------------
  @todo          | 2017/03/06  | 0.0.0.1   | D.C.P    | 创建文件             
******************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

#ifndef __INC_EXCEPTPUB_H__
#define __INC_EXCEPTPUB_H__
/******************************************************************
*   Name：头文件
*   Desc：主要是被外部调用的变量函数需要
******************************************************************/
#include "except.h"
/******************************************************************
*   Name：公共变量
*   Desc：主要是被外部调用的变量
******************************************************************/

/******************************************************************
*   Name：公共函数接口
*   Desc：可以被内部和外部调用的函数
******************************************************************/
/**
* @brief 注册回调函数
* @param [in] ExceptFunc 异常处理回调函数:void(*Handler_t)(void*)
* @retval FUNC_SUCC  表示成功
* @retval FUNC_FAIL  表示失败
*/
u32_t except_register_callback( Handler_t ExceptFunc, void *arg );

/******************************************************************
*   Name：公共接口宏
*   Desc：主要是被外部调用的宏接口，断言，各类异常
******************************************************************/
/**  
* @brief 断言宏 
* @param [in] Except 需判断的异常 
* @attention 当定义了NODEBUG就将debug模式转成release模式
*/
#define ASSERT( Except ) _ASSERT( Except ) 
/**  
* @brief 处理异常的宏 
* @param [in] err       错误码
* @param [in] desc      错误描述
*/
#define EXCEPTION( Err, Decs ) _EXCEPTION( Err, Decs )
#endif/*__INC_EXCEPTPUB_H__*/

#ifdef __cplusplus
}
#endif /* __cplusplus */
