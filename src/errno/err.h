/******************************************************************************
        (c) COPYRIGHT 2017-  by D.C.P  Co.,Ltd
                          All rights reserved.
@file: err.h
@brief:对于错误码的定义 接口函数的定义
@author   D.C.P 			                                                 
@version  0.0.0.1                                                               
@date     2017/03/02
-------------------------------------------------------------------------------
  Change History :                                                              
                 | <Date>      | <Version> | <Author> | <Description>                     
-------------------------------------------------------------------------------
  @todo          | 2017/03/02  | 0.0.0.1   | D.C.P    | 创建文件             
******************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

#ifndef __INC_ERR_H__
#define __INC_ERR_H__


/**  @brief 成功返回 */
#define ERR_SUCC FUNC_SUCC
/**  @brief 失败返回 */
#define ERR_FAIL FUNC_FAIL

/**  @brief 错误码结构体 */
typedef struct {
    u32_t       ErrCode;    /**< Operation not permitted 错误码*/
    string_t    ErrString;  /**< Operation not permitted 错误描述*/
}Err;
/******************************************************************
*   Name：私有接口宏 
*   Desc：内部调用的一些宏，主要是获得异常函数调用堆栈
******************************************************************/
/**  
* @brief 返回错误宏 
* @param [in] Errno 错误码
* @param [in] Return 返回ERR_FAIL或者ERR_NULL
* @attention glibc库并不初始化errno，默认为EWOULDBLOCK or EAGAIN
*/
#define _RETURN_ERR( Errno, Return ) \
do{\
    errno = Errno;\
    return Return;\
}while(0)

/**  
* @brief 返回错误宏 
* @param [in] Errno 错误码
* @param [in] Return 返回ERR_FAIL或者ERR_NULL* @param [in] Desc  直接打印的错误自定义描述
*/
#define _RETURN_ERRDESC( Errno, Return, Desc ) \
do{\
    printf("func:%s err:%s \n", __FUNCTION__ ,Desc);\
    errno = Errno;\
    return Return;\
}while(0)

#endif/*__INC_ERR_H__*/

#ifdef __cplusplus
}
#endif /* __cplusplus */
