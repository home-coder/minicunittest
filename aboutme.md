<table><tr><td bgcolor=#141F26>
<h1><font color=#fbc02d face="STKaiti">Mini CUnitTest</font></h1>

<h2><font color=#fbc02d face="STKaiti">简介</font></h2>

<h3><font color=#fbc02d face="STKaiti">mini单元测试框架主要用于C语言单元测试</font></h3>
>- <font color=#008080 face="STKaiti">**便捷性** ：</font><font color=#fbc02d face="STKaiti">本框架主要关联文件只有一个TEST.c所以文件更加轻便，搭建框架更加快速</font></br>
>- <font color=#008080 face="STKaiti">**多功能** ：</font><font color=#fbc02d face="STKaiti">且支持功能繁多，可以测试函数性能，展示覆盖率，对于内存泄漏也有侦查</font></br>
>- <font color=#008080 face="STKaiti">**易展示** ：</font><font color=#fbc02d face="STKaiti">展示结果是基于HTML5和CSS3开发，所以相对于以前的单元测试框架，这个更加美观，也易于理解</font></br>
>- <font color=#008080 face="STKaiti">**易使用** ：</font><font color=#fbc02d face="STKaiti">使用便捷， 只需要将test.py文件放入到待测试源码的下，简单的设置就可以完成</font></br>
-------------------


<h2><font color=#fbc02d face="STKaiti">功能</font></h2>
>- <font color=#fbc02d face="STKaiti">支持侦查测试的模块是否出现内存泄漏，并展示出来（仅支持linux，不支持mac）</font></br>
>- <font color=#fbc02d face="STKaiti">每个suite展示基本的 测试数 测试错误数，测试的时间</font></br>
>- <font color=#fbc02d face="STKaiti">每个suite里展示成功或者失败的结果 以及出错时候出错的所在case</font></br>
>- <font color=#fbc02d face="STKaiti">支持展示覆盖率 语句覆盖率 分支覆盖率 条件覆盖率 函数覆盖率</font></br>
>- <font color=#fbc02d face="STKaiti">支持通过代码形式查看覆盖率已经函数调用次数等</font></br>
>- <font color=#fbc02d face="STKaiti">支持函数性能，包括函数执行所需时间，函数被调用次数等性能(仅支持linux)</font></br>
>- <font color=#fbc02d face="STKaiti">支持环境 linux 嵌入式 mac(mac函数性能的功能和内存泄漏功能仍暂未支持)</font></br>
-------------------


<h2><font color=#fbc02d face="STKaiti">UintTest.py命令</font></h2>
>- <font color=#008080 face="STKaiti">**-h**</font> <font color=#fbc02d face="STKaiti">展示所有支持的命令</font>  <font color=#c0392b face="STKaiti">**[例子: python UnitTest.py -h ]**</font></br>
>- <font color=#008080 face="STKaiti">**-a**</font> <font color=#fbc02d face="STKaiti">配置好UnitTest.py后，自动编译出test文件</font> <font color=#c0392b face="STKaiti">**[例子: python UnitTest.py -a ]**</font></br>
>- <font color=#008080 face="STKaiti">**-e**</font> <font color=#fbc02d face="STKaiti">配置好UnitTest.py后，编译出ResultsFile文件，然后将UnitTest.py 和 ResultsFile 放入到嵌入式系统中执行</font> <font color=#c0392b face="STKaiti">**[例子: python UnitTest.py -e ResultsFile ]**</font></br>
>- <font color=#008080 face="STKaiti">**-m**</font> <font color=#fbc02d face="STKaiti">配置好UnitTest.py后，使用Makefile编译出ResultsFile文件 然后执行UnitTest.py</font>  <font color=#c0392b face="STKaiti">**[例子: python UnitTest.py -m Makefile ResultsFile ]**</font></br>
-------------------


<h2><font color=#fbc02d face="STKaiti">展示</font></h2>
<h3><font color=#fbc02d face="STKaiti">测试报告页面</font></h3>
<img src="./png/index.png" width = "718" height = "414" alt="图片名称" align=center />

<h3><font color=#fbc02d face="STKaiti">覆盖率页面</font></h3>
<img src="./png/performance.png" width = "718" height = "414" alt="图片名称" align=center />

<h3><font color=#fbc02d face="STKaiti">函数性能页面</font></h3>
<img src="./png/coverage.png" width = "718" height = "414" alt="图片名称" align=center />
-------------------


<h2><font color=#fbc02d face="STKaiti">使用方法</font></h2>
<h3><font color=#fbc02d face="STKaiti">文件目录关系图</font></h3>
<img src="./png/relation.png" width = "830" height = "410" alt="图片名称" align=center />

<h4><font color=#fbc02d face="STKaiti">1.放置UnitTest.py</font></h4>
> <font color=#fbc02d face="STKaiti">将UnitTest.py放在测试模块目录下</font>

<h4><font color=#fbc02d face="STKaiti">2.配置UnitTest.py</font></h4>
> <font color=#fbc02d face="STKaiti">将 UnitPath="./" 修改为单元测试目录所在路径</font> <font color=#c0392b face="STKaiti">**[例如:UnitPath="/中间目录/src/ 以/结尾"]**</font></br>
> <font color=#fbc02d face="STKaiti">将 FilePath="./"  修改为待测试文件目录所在路径</font> <font color=#c0392b face="STKaiti">**[例如:UnitPath="/中间目录/xxx/  以/结尾"]**</font></br>

<h4><font color=#fbc02d face="STKaiti">3.配置UnitTest.py</font></h4>
> - <font color=#fbc02d face="STKaiti">-a模式 这个模式自动进行gcc编译，将TestFile = ["test_TEST.c"]; 这句中添加所需要编译的文件，这些文件也是被测试文件</font>
    <font color=#c0392b face="STKaiti">**(例如:TestFile =["aaa/TestingA.c", "aaa/TestingB.c", "bbb/Tmp.c"] )**</font></br>
> - <font color=#fbc02d face="STKaiti">-e模式 将TestFile = ["test_TEST.c"]; 这句中添加需要测试的文件，具体使用和-a模式一样。然后将src文件UnitTest.py，以及待测试文件，执行文件放入到
    嵌入式系统中，按照上面的图示关系，然后执行UnitTest.py -e /中间目录/xxx/Resultfile</font></br>
> - <font color=#fbc02d face="STKaiti">-m模式 将TestFile = ["test_TEST.c"]; 这句中添加需要测试的文件，具体使用和-a模式一样。按照上面的图示关系，执行UnitTest.py -m /中间目录/xxx/makefile /中间目录/xxx/Resultfile</font>
    <font color=#c0392b face="STKaiti">**[makefile的gcc当中要加入编译参数 -fprofile-arcs -ftest-coverage -pg]**</font></br>

<h4><font color=#fbc02d face="STKaiti">4.关联TEST.c文件</font></h4>
> <font color=#fbc02d face="STKaiti">使用-e -m 模式时候 将TEST.c文件关联到makefile里面</font>

<h4><font color=#fbc02d face="STKaiti">5.执行UnitTest.py</font></h4>
-------------------


<h2><font color=#fbc02d face="STKaiti">问题</font></h2>
-------------------


<h2><font color=#fbc02d face="STKaiti">联系</font></h2>
<font color=#c0392b size=5 face="STKaiti">**当出现问题以及bug时欢迎联系 邮箱:garsonzhang@foxmail.com**</font>
-------------------
</td></tr></table>
